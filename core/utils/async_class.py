from abc import ABC
from typing import Generator, Generic, TypeVar

Class = TypeVar("Class")


class Async(Generic[Class], ABC):
    """Добавляет возможность писать асинхронный конструктор"""

    async def __ainit__(self):
        """Асинхронный конструктор блока"""
        return self

    def __await__(self) -> Generator[None, None, Class]:
        return self.__ainit__().__await__()
