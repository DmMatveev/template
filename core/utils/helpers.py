from itertools import chain
from typing import Dict, List, Sequence, TypeVar, Union

Type = TypeVar("Type")
Key = TypeVar("Key")


def to_plain_list(objects: List[List[Type]]) -> List[Type]:
    return list(chain.from_iterable(objects))


def list_map_objects_by_key(objects: List[Type], key: str) -> Dict[Union[str, int], List[Type]]:
    map_key_to_objects = {}
    for obj in objects:
        map_key_to_objects.setdefault(getattr(obj, key), []).append(obj)

    return map_key_to_objects


def get_objects_by_ids(objects_ids: List[Key], map_object_id_to_object: Dict[Key, Type]) -> List[Type]:
    return [map_object_id_to_object[object_id] for object_id in objects_ids]
