import asyncio
import logging
from asyncio import Task
from concurrent.futures import CancelledError
from typing import Coroutine

task_logger = logging.getLogger("main")


def create_task(coroutine: Coroutine) -> Task:
    def done_callback(task: Task) -> None:
        try:
            task.result()
        except Exception as e:  # noqa
            task_logger.exception(e)
        except CancelledError:
            return

    task = asyncio.create_task(coroutine)
    task.add_done_callback(done_callback)

    return task
