from typing import Dict, Generic, TypeVar

import orjson
from utils import factory

Dataclass = TypeVar("Dataclass")


class SerializeException(Exception):
    pass


class JSONMixin(Generic[Dataclass]):
    @classmethod
    def from_bytes(cls, json: bytes) -> Dataclass:
        try:
            json = orjson.loads(json)
            return cls.from_dict(json)
        except orjson.JSONDecodeError as e:
            raise SerializeException from e

    @classmethod
    def from_dict(cls, json: Dict) -> Dataclass:
        try:
            return factory.load(json, cls)
        except Exception as e:
            raise SerializeException from e

    def to_dict(self) -> Dict:
        return factory.dump(self, self.__class__)

    def to_bytes(self) -> bytes:
        json = self.to_dict()
        return orjson.dumps(json)
