from datetime import date, datetime, timedelta, timezone

DateTime = datetime
Date = date
TimeZone = timezone


class DateTimeHelper:
    @classmethod
    def set_timezone_from_city(cls, utc_datetime: DateTime) -> DateTime:
        return utc_datetime

    @classmethod
    def get_now_date(cls) -> Date:
        return cls.get_now_datetime().date()

    @classmethod
    def get_now_datetime(cls) -> DateTime:
        return datetime.now(tz=cls.get_timezone_from_city())

    @classmethod
    def get_tomorrow_date(cls) -> Date:
        return cls.get_now_date() + timedelta(days=1)

    @classmethod
    def is_tomorrow_datetime(cls, utc_datetime: DateTime) -> bool:
        current_datetime_with_timezone = cls.set_timezone_from_city(utc_datetime)
        current_date = current_datetime_with_timezone.date()

        tomorrow_date = cls.get_tomorrow_date()

        return (tomorrow_date - current_date).days == 0

    @classmethod
    def is_tomorrow_date(cls, current_date: Date) -> bool:
        tomorrow_date = cls.get_tomorrow_date()
        return (tomorrow_date - current_date).days == 0

    @classmethod
    def is_now_date(cls, current_date: Date) -> bool:
        now = cls.get_now_date()
        return (current_date - now).days == 0

    @classmethod
    def get_timezone_from_city(cls) -> TimeZone:
        return timezone.utc
