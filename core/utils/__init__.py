from base64 import b64decode
from datetime import datetime, time

import pendulum
from dataclass_factory import Factory, Schema
from pendulum import DateTime

factory = Factory(
    default_schema=Schema(omit_default=True),
    schemas={
        datetime: Schema(
            parser=pendulum.parse,
            serializer=datetime.isoformat,
        ),
        DateTime: Schema(serializer=datetime.isoformat),
        time: Schema(parser=lambda x: time.fromisoformat(x) if type(x) == str else x, serializer=time.isoformat),
        bytes: Schema(parser=lambda value: b64decode(value.encode())),
    },
)
