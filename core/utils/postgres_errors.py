from psycopg2 import errors

UniqueViolation = errors.lookup("23505")
ForeignKeyViolation = errors.lookup("23503")
