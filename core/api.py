import sentry_sdk
from config.env import DEBUG, ENVIRONMENT, SENTRY_DSN
from endpoints import routes
from infrastructures.db import Database
from infrastructures.rabbit import Rabbit
from sentry_asgi import SentryMiddleware
from starlette.applications import Starlette

sentry_sdk.init(dsn=SENTRY_DSN, environment=ENVIRONMENT)


app = Starlette(
    debug=DEBUG,
    routes=routes,
    on_startup=[
        Database.start,
        Rabbit.start,
    ],
    on_shutdown=[
        Database.stop,
        Rabbit.stop,
    ],
)

# Middleware
app.add_middleware(SentryMiddleware)


if __name__ == "__main__":
    from argparse import ArgumentParser

    import uvicorn

    parser = ArgumentParser()
    parser.add_argument("--host", type=str, default="127.0.0.1", help="Enter port")
    parser.add_argument("--port", type=int, default=8001, help="Enter port")
    args = parser.parse_args()

    uvicorn.run(
        "api:app",
        host=args.host,
        loop="uvloop",
        debug=DEBUG,
        reload=DEBUG,
        port=args.port,
    )
