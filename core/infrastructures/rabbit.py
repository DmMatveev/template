from typing import Optional

import aio_pika
import orjson
from aio_pika import Channel
from aio_pika.connection import ConnectionType
from config.env import (
    RABBIT_HOST,
    RABBIT_LOGIN,
    RABBIT_PASSWORD,
    RABBIT_PORT,
    RABBIT_VHOST,
)


class Rabbit:
    _rabbit: Optional[ConnectionType] = None

    @classmethod
    async def send_message(cls, data: dict, queue_name: str, planning_type: str) -> None:
        channel = await cls.get_channel()
        await channel.default_exchange.publish(
            aio_pika.Message(body=orjson.dumps(data), headers={"planning_type": planning_type}),
            routing_key=queue_name,
        )

    @classmethod
    async def get_channel(cls) -> Channel:
        return await cls._rabbit.channel()

    @classmethod
    async def start(cls) -> None:
        if not cls._rabbit:
            cls._rabbit = await aio_pika.connect_robust(
                host=RABBIT_HOST,
                port=RABBIT_PORT,
                login=RABBIT_LOGIN,
                password=RABBIT_PASSWORD,
                virtualhost=RABBIT_VHOST,
            )

    @classmethod
    async def stop(cls) -> None:
        if cls._rabbit:
            await cls._rabbit.close()
            del cls._rabbit
