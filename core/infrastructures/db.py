from contextlib import asynccontextmanager
from contextvars import ContextVar
from typing import AsyncIterator, Optional

import aiopg
from aiopg import Cursor, Pool, Transaction
from config.env import (
    POSTGRES_DB_HOST,
    POSTGRES_DB_NAME,
    POSTGRES_DB_PASSWORD,
    POSTGRES_DB_PORT,
    POSTGRES_DB_USER,
)
from more_itertools import first
from psycopg2.extras import DictCursor


class DatabaseException(Exception):
    pass


class ReturnMoreOneRowDatabaseException(DatabaseException):
    pass


class NoRowsDatabaseException(DatabaseException):
    pass


class DNSHelper:
    @classmethod
    def get_dns(cls) -> str:
        args = {
            "dbname": POSTGRES_DB_NAME,
            "user": POSTGRES_DB_USER,
            "password": POSTGRES_DB_PASSWORD,
            "host": POSTGRES_DB_HOST,
            "port": POSTGRES_DB_PORT,
        }

        return " ".join([f"{arg_name}={arg_value}" for arg_name, arg_value in args.items()])


class Database:
    __pool: Optional[Pool] = None
    __cursor: ContextVar[Optional[Cursor]] = ContextVar("cursor", default=None)
    __transaction: ContextVar[Optional[Transaction]] = ContextVar("transaction", default=None)

    @classmethod
    async def start(cls) -> None:
        if not cls.__pool:
            cls.__pool = await aiopg.create_pool(DNSHelper.get_dns())

    @classmethod
    async def stop(cls) -> None:
        if cls.__pool:
            cls.__pool.close()
            await cls.__pool.wait_closed()
            cls.__pool = None

    @classmethod
    @asynccontextmanager
    async def transaction(cls) -> AsyncIterator[Transaction]:
        try:
            if transaction := cls.__transaction.get():
                yield transaction
            else:
                async with cls.__get_cursor() as cursor:
                    cls.__cursor.set(cursor)

                    async with cursor.begin() as transaction:
                        cls.__transaction.set(transaction)
                        yield transaction

        finally:
            cls.__transaction.set(None)
            cls.__cursor.set(None)

    @classmethod
    async def get_rows(cls, query: str, **kwargs) -> list[dict]:
        async with cls.__get_cursor() as cursor:
            await cursor.execute(query, kwargs)
            rows = await cursor.fetchall()
            return list(map(dict, rows))

    @classmethod
    async def get_row(cls, query: str, **kwargs) -> dict:
        rows = await cls.get_rows(query, **kwargs)

        if len(rows) == 1:
            return first(rows)
        elif len(rows) > 1:
            raise ReturnMoreOneRowDatabaseException
        else:
            raise NoRowsDatabaseException

    @classmethod
    async def get_json(cls, query: str, **kwargs) -> dict:
        row = await cls.get_row(query, **kwargs)
        return first(list(row.values()))

    @classmethod
    async def bulk(cls, sql: str, args: list[dict]) -> None:
        if not args:
            return

        async with cls.__get_cursor() as cursor:
            sql = b";".join([cursor.mogrify(sql, arg) for arg in args])
            await cursor.execute(sql)

    @classmethod
    async def execute(cls, query: str, **kwargs) -> None:
        async with cls.__get_cursor() as cursor:
            await cursor.execute(query, kwargs)

    @classmethod
    @asynccontextmanager
    async def __get_cursor(cls) -> AsyncIterator[Cursor]:
        if cursor := cls.__cursor.get():
            yield cursor
        else:
            async with cls.__pool.acquire() as connection:
                async with connection.cursor(cursor_factory=DictCursor) as cursor:
                    yield cursor
