from endpoints.health_check import health_endpoint
from starlette.routing import Mount, Route

from . import reports, v1  # noqa ABS101

routes = [
    Route("/health", endpoint=health_endpoint),
    Mount("/reports", routes=reports.routes),
    Mount("/v1", routes=v1.routes),
]
