from starlette.requests import Request
from starlette.responses import PlainTextResponse


async def health_endpoint(request: Request) -> PlainTextResponse:  # noqa U100
    return PlainTextResponse("ok")
