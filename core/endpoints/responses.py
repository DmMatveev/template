from typing import Dict

import orjson
from starlette.responses import JSONResponse
from tms_client_v1.api.models.error import ErrorModel


class OrjsonResponse(JSONResponse):
    def render(self, content: Dict) -> bytes:
        return orjson.dumps(content)


class InvalidResponse(OrjsonResponse):
    def __init__(
        self,
        content: ErrorModel,
        status_code: int = 400,
    ) -> None:
        super().__init__(content, status_code)

    def render(self, content: ErrorModel) -> bytes:
        return super().render(content.to_dict())


class FailureResponse(InvalidResponse):
    def __init__(
        self,
        content: ErrorModel,
        status_code: int = 500,
    ) -> None:
        super().__init__(content, status_code)
