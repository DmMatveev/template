from pathlib import Path

from starlette.config import Config

from library.common import Environment

ROOT_DIR = Path(__file__).resolve().parent.parent.parent

config = Config(f"{ROOT_DIR / '.env'}")

DEBUG = config("DEBUG", cast=bool, default=False)

SENTRY_DSN = config("SENTRY_DSN", cast=str, default="")

POSTGRES_DB_NAME = config("POSTGRES_DB_NAME", cast=str, default="tms_math")
POSTGRES_DB_USER = config("POSTGRES_DB_USER", cast=str, default="postgres")
POSTGRES_DB_PASSWORD = config("POSTGRES_DB_PASSWORD", cast=str, default="postgres")
POSTGRES_DB_HOST = config("POSTGRES_DB_HOST", cast=str, default="localhost")
POSTGRES_DB_PORT = config("POSTGRES_DB_PORT", cast=int, default=5432)

ENVIRONMENT = config("ENVIRONMENT", cast=Environment, default=Environment.LOCAL)
