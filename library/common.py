from library.enums import StrEnum


class Environment(StrEnum):
    LOCAL = "local"
    DEVELOPMENT = "dev"
    RELEASE = "stage"
    PRODUCTION = "prod"
