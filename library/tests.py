from typing import Generic, List, TypeVar

from factory.base import BaseFactory, BaseMeta, FactoryMetaClass

Schema = TypeVar("Schema")


class CustomFactoryMeta(FactoryMetaClass):
    def __call__(cls):
        raise NotImplementedError("__call__ is not implemented, recommend to use method 'create' or 'create_batch'")


class CustomFactoryBase(BaseFactory, metaclass=CustomFactoryMeta):
    class Meta(BaseMeta):
        pass


class Factory(Generic[Schema], CustomFactoryBase):
    @classmethod
    def create(cls, *args, **kwargs) -> Schema:
        return super().create(**kwargs)

    @classmethod
    def create_batch(cls, size, **kwargs) -> List[Schema]:
        return super().create_batch(size, **kwargs)
