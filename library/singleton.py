class Singleton(type):
    __instance = None

    def __call__(cls, *args, **kw):
        if not cls.__instance:
            cls.__instance = super(Singleton, cls).__call__(*args, **kw)
        return cls.__instance
