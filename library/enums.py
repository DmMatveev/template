from enum import Enum as SystemEnum


class StrEnum(str, SystemEnum):
    def __str__(self) -> str:
        return self.value

    def _generate_next_value_(self, start, count, last_values) -> str:  # noqa
        return self
