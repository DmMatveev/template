import asyncio
from logging import Logger
from typing import Dict, Optional, Union

import orjson
from aiohttp import ClientSession, ClientTimeout, FormData
from yarl import URL

from library.client.enums import RequestMethodType
from library.client.exceptions import (
    HostNotSetClientException,
    JSONInvalidClientException,
    LoggingNotSetClientException,
    RequestClientException,
    TimeoutClientException,
)
from library.client.types import StrOrURL


class AsyncClient:
    HOST: StrOrURL = None
    LOGGER: Logger = None

    def __init__(self, host: StrOrURL = None, logger: Logger = None) -> None:
        self._session: Optional[ClientSession] = None

        host = self.HOST or host
        if not host:
            raise HostNotSetClientException

        logger = self.LOGGER or logger
        if not logger:
            raise LoggingNotSetClientException

        self.HOST = URL(host)
        self.LOGGER = logger

    async def start(self) -> None:
        if not self._session:
            self._session = ClientSession()

    async def stop(self) -> None:
        if self._session:
            await self._session.close()
            self._session = None

    async def _api_call(  # noqa: CFQ002
        self,
        *,
        method: RequestMethodType,
        endpoint: StrOrURL,
        json: Dict = None,
        body: Union[bytes, FormData] = None,
        headers: Dict = None,
        timeout: ClientTimeout = None,
    ) -> Dict:
        url = self.HOST.join(URL(endpoint))

        response = await self.__request(
            method=method,
            url=url,
            json=json,
            body=body,
            headers={"Content-Type": "application/json", **(headers or {})},
            timeout=timeout,
        )

        try:
            json = orjson.loads(response)
        except orjson.JSONDecodeError:
            exception = JSONInvalidClientException(
                method=method,
                url=url,
                headers=headers,
                body=body,
                status_code=200,
                content=response,
            )
            self.LOGGER.error(
                "%s %s JSONInvalidClientException",
                exception.method,
                exception.url,
            )
            raise self._clean_exception(exception)

        return json

    async def _request(  # noqa: CFQ002
        self,
        *,
        method: RequestMethodType,
        endpoint: StrOrURL,
        json: Dict = None,
        body: Union[bytes, FormData] = None,
        headers: Dict = None,
        timeout: ClientTimeout = None,
    ) -> bytes:
        url = self.HOST.join(URL(endpoint))

        return await self.__request(
            method=method,
            url=url,
            json=json,
            body=body,
            headers=headers,
            timeout=timeout,
        )

    def _clean_exception(self, exception: RequestClientException) -> RequestClientException:
        return exception

    async def __request(  # noqa: CFQ002
        self,
        method: RequestMethodType,
        url: URL,
        json: Dict = None,
        body: Union[bytes, FormData] = None,
        headers: Dict = None,
        timeout: ClientTimeout = None,
    ) -> bytes:  # noqa

        if json:
            body = orjson.dumps(json)

        self.LOGGER.info("[%s] %s", method, url)

        try:
            response = await self._session.request(
                method,
                url,
                headers=headers,
                data=body,
                timeout=timeout,
            )
        except asyncio.TimeoutError:
            exception = TimeoutClientException(
                method=method,
                url=url,
                headers=headers,
                body=body,
                status_code=502,
            )
            self.LOGGER.error(
                "%s %s TimeoutClientException",
                exception.method,
                exception.url,
            )
            raise self._clean_exception(exception)

        self.LOGGER.info("[%s] %s %s", method, response.status, url)

        content = await response.read()

        # Return connection to POOL
        response.release()

        if 400 <= response.status:
            exception = RequestClientException(
                method=method,
                url=url,
                headers=headers,
                body=body,
                status_code=response.status,
                content=content,
            )
            self.LOGGER.error(
                "%s %s %s ClientRequestException",
                exception.method,
                exception.url,
                exception.status_code,
            )
            raise self._clean_exception(exception)

        return content
