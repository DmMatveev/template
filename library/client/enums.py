from enum import auto

from library.enums import StrEnum


class RequestMethodType(StrEnum):
    POST = auto()
    GET = auto()
    DELETE = auto()
