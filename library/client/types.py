from typing import Union

from yarl import URL

StrOrURL = Union[str, URL]
