from typing import Optional

from yarl import URL

from library.client.enums import RequestMethodType


class ClientException(Exception):
    pass


class HostNotSetClientException(ClientException):
    pass


class LoggingNotSetClientException(ClientException):
    pass


class RequestClientException(ClientException):
    def __init__(  # noqa: CFQ002
        self,
        method: RequestMethodType,
        url: URL,
        headers: Optional[dict],
        body: Optional[bytes],
        status_code: Optional[int] = None,
        content: Optional[bytes] = None,
    ) -> None:
        self.method = method
        self.url = url
        self.headers = headers
        self.body = body
        self.status_code = status_code
        self.content = content

    def __str__(self) -> str:
        return f"[{self.method}] {self.url} -> [{self.status_code}] ClientRequestException"


class TimeoutClientException(RequestClientException):
    def __str__(self) -> str:
        return f"[{self.method}]{self.url} -> TimeoutClientException"


class JSONInvalidClientException(RequestClientException):
    def __str__(self) -> str:
        return f"[{self.method}]{self.url} -> JSONInvalidClientException"
