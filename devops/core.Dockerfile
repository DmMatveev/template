FROM registry.gl.sdvor.com/tms/tms-math/base:v.base AS build

COPY pyproject.toml pyproject.toml
COPY poetry.lock poetry.lock

RUN poetry install --no-dev --extras "core"

FROM python:3.9.5-slim-buster

WORKDIR /tms-math

COPY --from=build /usr/local /usr/local
COPY src .
COPY library ./library
