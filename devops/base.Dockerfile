FROM python:3.9.5-slim-buster AS build

ENV PATH="/root/.poetry/bin:$PATH"

RUN apt-get update \
&&  apt-get dist-upgrade -y \
&&  apt-get install curl -y \
&&  curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python - \
&&  apt-get purge curl -y \
&&  apt-get autoremove -y \
&&  rm -rf /var/lib/apt/lists/* \
&&  poetry config virtualenvs.create false
