# Как меня запустить?
1. Установить poetry и Python 3.9
2. ```poetry install --extras "core admin develop"```
   </br>
   __develop__ - библиотеки для локального разработки
   </br>
   __core__ - зависимости api(Starlette)
   </br>
   __admin__ - зависимости admin(Django)

3. Добавить .env в корень проекта. [Пример](.env-example)

# Setup pre-commit
1. Устанавливаем все зависимости.
2. Установка ```pre-commit install```