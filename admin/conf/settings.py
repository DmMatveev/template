from pathlib import Path

import sentry_sdk
from environs import Env
from sentry_sdk.integrations.django import DjangoIntegration

from library.common import Environment

BASE_DIR = Path(__file__).resolve().parent.parent
ROOT_DIR = BASE_DIR.parent

env = Env()
env.read_env(path=f"{BASE_DIR / '.env'}")

SENTRY_DSN = env("SENTRY_DSN", cast=str, default="")

SERVICE_NAME = env("SERVICE_NAME", default="")
ENVIRONMENT = env("ENVIRONMENT", cast=Environment, default=Environment.LOCAL)

sentry_sdk.init(
    dsn=SENTRY_DSN,
    environment=ENVIRONMENT,
    integrations=[DjangoIntegration()],
)

SECRET_KEY = env("DJANGO_SECRET", cast=str, default="secret")

DEBUG = env("DEBUG", cast=bool, default=False)

ALLOWED_HOSTS = ["*"]

DJANGO_CONTRIB = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
]

INSTALLED_APPS = [
    "django_json_widget",
    "nested_admin",
    "import_export",
]

APPS = [
    "apps.user",
]

INSTALLED_APPS += DJANGO_CONTRIB + APPS

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "conf.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "conf.wsgi.application"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": env("POSTGRES_DB_NAME", cast=str, default="tms_math"),
        "USER": env("POSTGRES_DB_USER", cast=str, default="postgres"),
        "PASSWORD": env("POSTGRES_DB_PASSWORD", cast=str, default="postgres"),
        "HOST": env("POSTGRES_DB_HOST", cast=str, default=""),
        "PORT": env("POSTGRES_DB_PORT", cast=str, default=""),
        "CONN_MAX_AGE": 100,
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

LANGUAGE_CODE = "ru"

TIME_ZONE = "UTC"

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = "/static/"
STATIC_ROOT = BASE_DIR / "static"

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"


if DEBUG:
    INSTALLED_APPS.append("debug_toolbar")
    MIDDLEWARE.append("debug_toolbar.middleware.DebugToolbarMiddleware")

    INTERNAL_IPS = ["127.0.0.1"]
