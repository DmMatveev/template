#!/usr/bin/env python
import os
import sys

from conf import settings


def main():
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "conf.settings")
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc

    local_hosts = ["localhost", "postgres-service.default", "host.docker.internal"]

    is_migrate_op = "migrate" in sys.argv
    is_remote_db = (
        settings.DATABASES["default"]["HOST"] not in local_hosts or settings.DATABASES["default"]["PORT"] != "5432"
    )

    if is_migrate_op and is_remote_db:
        raise Exception("`migrate` operation only allowed on local database (5432 port) ")

    execute_from_command_line(sys.argv)


if __name__ == "__main__":
    main()
