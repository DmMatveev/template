from random import randint

from apps.health.models import TestModel
from django.db import DatabaseError, IntegrityError
from django.http import HttpResponse
from django.views import View


class HealthView(View):
    def get(self, request):  # noqa
        try:
            model = TestModel.objects.create(value=randint(0, 100000))
            model.delete()
        except (IntegrityError, DatabaseError):
            return HttpResponse(status=500)

        return HttpResponse(status=200)
