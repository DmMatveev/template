from django.db import models


class TestModel(models.Model):
    value = models.IntegerField()

    class Meta:
        db_table = "django_health"
