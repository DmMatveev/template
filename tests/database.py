import pytest


@pytest.mark.asyncio()
@pytest.mark.django_db(transaction=True, reset_sequences=True)
class DatabaseTest:
    @pytest.fixture(autouse=True)
    async def __setup_connect(self):
        from infrastructures.db import Database

        await Database.start()
        yield
        await Database.stop()
