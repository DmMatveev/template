import datetime

from factory.fuzzy import FuzzyDateTime as SystemFuzzyDateTime
from factory.fuzzy import FuzzyFloat as SystemFuzzyFloat
from factory.fuzzy import FuzzyInteger as SystemFuzzyInteger


class FuzzyDateTime(SystemFuzzyDateTime):
    def __init__(self, **kwargs):
        start_dt = datetime.datetime(2008, 1, 1, tzinfo=datetime.timezone.utc)
        end_dt = datetime.datetime(2045, 1, 1, tzinfo=datetime.timezone.utc)
        super().__init__(start_dt, end_dt, force_microsecond=0, **kwargs)

    def fuzz(self):
        result: datetime.datetime = super().fuzz()
        return result

    def _now(self):
        _now = datetime.datetime.now(tz=datetime.timezone.utc)
        return _now


class FuzzyTime(FuzzyDateTime):
    def fuzz(self):
        return super().fuzz().time()

    def _now(self):
        return super()._now().time()


class FuzzyBool(SystemFuzzyInteger):
    def __init__(self):
        super().__init__(0, 1)

    def fuzz(self):
        return bool(super().fuzz())


class FuzzyInteger(SystemFuzzyInteger):
    def __init__(self):
        super().__init__(0, 1000)


class FuzzyFloat(SystemFuzzyFloat):
    def __init__(self):
        super().__init__(0, 1000, 2)
