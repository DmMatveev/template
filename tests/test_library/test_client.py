import logging

import orjson
import pytest
from _pytest.logging import LogCaptureFixture
from aiohttp import ClientTimeout
from aioresponses import aioresponses
from more_itertools import first
from yarl import URL

from library.client.async_client import AsyncClient
from library.client.enums import RequestMethodType
from library.client.exceptions import (
    JSONInvalidClientException,
    RequestClientException,
    TimeoutClientException,
)

TEST_HOST = "https://www.test.com"


@pytest.fixture()
async def client() -> AsyncClient:
    client = AsyncClient(URL(TEST_HOST), logging.getLogger("Test"))
    await client.start()

    yield client

    client.BASE_URL = None
    await client.stop()


@pytest.mark.asyncio()
class TestClient:
    VALID_JSON = '{"STATUS": "SUCCESS"}'
    INVALID_JSON = '{"STATUS": "SUCCESS"'

    async def test__request_success(self, client: AsyncClient) -> None:
        """Запрос с ответом < 400"""

        with aioresponses() as mock:
            mock.get(TEST_HOST, status=200, body=self.VALID_JSON)

            content = await client._AsyncClient__request(  # noqa
                method=RequestMethodType.GET,
                url=TEST_HOST,
            )

            assert content.decode() == self.VALID_JSON

    async def test__request_failure(self, client: AsyncClient, caplog: LogCaptureFixture) -> None:
        """Запрос с ответом >= 400"""

        with aioresponses() as mock:
            mock.get(TEST_HOST, status=400, body=self.VALID_JSON)

            with pytest.raises(RequestClientException):
                content = await client._AsyncClient__request(  # noqa
                    method=RequestMethodType.GET,
                    url=TEST_HOST,
                )

        assert len(caplog.messages) == 1
        assert first(caplog.messages) == "GET https://www.test.com 400 ClientRequestException"

    async def test__timeout_error(self, client: AsyncClient, caplog: LogCaptureFixture) -> None:
        """Timeout запроса"""

        with aioresponses() as mock:
            mock.get(TEST_HOST, timeout=True)

            with pytest.raises(TimeoutClientException):
                content = await client._AsyncClient__request(  # noqa
                    method=RequestMethodType.GET,
                    url=TEST_HOST,
                    timeout=ClientTimeout(1),
                )

        assert len(caplog.messages) == 1
        from more_itertools import first

        assert first(caplog.messages) == "GET https://www.test.com TimeoutClientException"

    async def test__valid_json(self, client: AsyncClient) -> None:
        """Клиент ответил валидным JSON"""

        with aioresponses() as mock:
            mock.get(TEST_HOST, status=200, body=self.VALID_JSON)

            content = await client._api_call(  # noqa
                method=RequestMethodType.GET,
                endpoint="",
            )

            assert content == orjson.loads(self.VALID_JSON)

    async def test__invalid_json(self, client: AsyncClient, caplog: LogCaptureFixture) -> None:
        """Клиент ответил не валидным JSON"""

        with aioresponses() as mock:
            mock.get(TEST_HOST, status=200, body=self.INVALID_JSON)

            with pytest.raises(JSONInvalidClientException):
                content = await client._api_call(  # noqa
                    method=RequestMethodType.GET,
                    endpoint="",
                )

        assert len(caplog.messages) == 1
        assert first(caplog.messages) == "GET https://www.test.com JSONInvalidClientException"
