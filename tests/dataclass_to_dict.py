from dataclasses import asdict
from typing import Any, Iterable


def dict_factory_which_transform_not_init_to_none(results: list[tuple[Any, Any]], excluded: Iterable[str]) -> dict:
    for result in results.copy():
        field_name = result[0]
        is_in_excluded = field_name in excluded
        if is_in_excluded:
            results.remove(result)
            results.append((result[0], None))

    return dict(results)


def dataclass_to_dict(obj: Any, excluded: Iterable[str] = ()) -> dict:
    return asdict(obj, dict_factory=lambda result: dict_factory_which_transform_not_init_to_none(result, excluded))


def dataclasses_to_dicts(objects: list[Any], excluded: Iterable[str] = ()) -> list[dict]:
    return list(map(lambda object: dataclass_to_dict(object, excluded), objects))
