from typing import Callable, Dict, Optional

import orjson
from aiohttp import hdrs
from aioresponses import aioresponses


class aiohttp_mock(aioresponses):
    def add(
        self,
        url: "Union[URL, str, Pattern]",
        method: str = hdrs.METH_GET,
        status: int = 200,
        body: Dict = "",
        exception: "Exception" = None,
        content_type: str = "application/json",
        payload: Dict = None,
        headers: Dict = None,
        response_class: "ClientResponse" = None,
        repeat: bool = False,
        timeout: bool = False,
        reason: Optional[str] = None,
        callback: Optional[Callable] = None,
    ) -> None:

        body = orjson.dumps(body).decode()

        super().add(
            url,
            method,
            status,
            body,
            exception,
            content_type,
            payload,
            headers,
            response_class,
            repeat,
            timeout,
            reason,
            callback,
        )
