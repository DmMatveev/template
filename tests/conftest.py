import pytest
from django.conf import settings
from starlette.config import environ

environ["POSTGRES_DB_NAME"] = "test_" + settings.DATABASES["default"]["NAME"]


@pytest.fixture()
async def database() -> None:
    from infrastructures.db import Database

    await Database.start()
    yield
    await Database.stop()
